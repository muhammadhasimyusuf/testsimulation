<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Choicelist extends Model
{
    protected $table = "choicelists";
    protected $guarded = ['id'];


    public function question(){
        return $this->belongsTo(Question::class, 'no', 'qno');
    }
}
