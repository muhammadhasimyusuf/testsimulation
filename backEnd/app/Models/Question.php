<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Question extends Model
{
    protected $table = "mquestion";
    protected $guarded = ['id'];


    public function listchoice(){
        return $this->hasMany(Choicelist::class, 'qno', 'no');
    }

    public function answered()
    {
        return $this->hasOne(Answer::class, 'qno', 'no');
    }
}
