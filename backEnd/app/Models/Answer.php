<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Answer extends Model
{
    protected $table = 'answered';
    protected $primaryKey = 'qid';
    protected $fillable = ['qno', 'nim', 'qlist', 'qanswer'];


    public function user(){
        return $this->belongsTo(User::class, 'nim', 'nim');
    }

    public function question(){
        return $this->belongsTo(Question::class, 'no', 'qno');
    }
}
