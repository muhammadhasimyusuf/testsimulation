<?php

namespace App\Http\Controllers;

use App\Models\Answer;
use App\Models\Question;
use App\Models\User;
use Illuminate\Database\Eloquent\Scope;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class UserController extends Controller
{
    public function create(Request $request)
    {
        $getUser = User::where('nim', $request->nim)->first();
        if($getUser){
            return response()->json(["result" => (object)[], "status" => "FAILED","response" => 400, "error" => true,],400);
        }

       $user = User::create($request->all());
       return response()->json(['message' => 'data added', 'data' => $user]);
    }

    public function login(Request $request)
    {
       $user = User::where('nim', $request->nim)->first();

       $isPassValid =  Hash::check($request->password, $user->password);

       if(!$user){
            return response()->json(['message' => 'data not found']);
       }

       if (!$isPassValid) {
        return response()->json(["result" => (object)[], "status" => "FAILED","response" => 400, "error" => true,],400);
    }

        return response()->json(["result" => (object)$user, "status" => "SUCCESS","response" => 200, "error" => false, "from" => $request ],200);
    }



    public function getanswered($nim, $qno)
    {
        $answer = Answer::where('nim', $nim)->where('qno',$qno)->first();
        // return $answer;
        if($answer){
        return response()->json(["result" => (object)$answer, "status" => "SUCCESS","response" => 200, "error" => false,],200);
    }else{
        return response()->json(["result" => (object)$answer, "status" => "NOT ANSWERED","response" => 400, "error" => true,],400);

    }
    }


    public function getscore($nim)
    {
        $user = User::where('nim', $nim)->first();
        $answer = Answer::where('nim', $nim)->get()->keyBy('qno');
        $question = Question::where('type', $user->qtype)->get()->keyBy('no');
        $scores = 0;
        $resultdata = [];
        // if(count($answer) < 50){
        //     return response()->json(["result" => "Soal belum selesai", "status" => "ERROR","response" => 200, "error" => true,],200);
        // }
        foreach($question as $key=>$q){
            $resultdata[] = (object)[
                'no' => $q->no,
                "question" => $q->question,
                "answer" => $answer[$key]->qanswer ?? '',
                "true" => $q->answer,
                "result" => isset($answer[$key]) ? $answer[$key]->qanswer == $q->answer : false
            ];
             if(isset($answer[$key]) && $answer[$key]->qanswer == $q->answer){
                $scores += 2;
             }


        };

        $data = (object) [
            "scores"=> $scores,
            "answer" => $resultdata,

        ];

        return response()->json(["result" =>$data, "status" => "SUCCESS","response" => 200, "error" => false,],200);
    }
    public function finishtest($nim)
    {
        $user = User::where('nim', $nim)->first();

        $updated = $user->update([
            "isFinish" => (int) '1',
        ]);

        if($updated){
        return response()->json(["result" => (object)$user, "status" => "SUCCESS","response" => 200, "error" => false],200);

        }

    }

}
