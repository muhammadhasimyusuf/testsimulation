<?php

namespace App\Http\Controllers;

use App\Models\Question;
use Illuminate\Http\Request;

use function PHPSTORM_META\type;

class MainController extends Controller
{
    public function main(){

        $data = Question::select('type', 'question')
        ->where('type', 'a')
        ->get();


        return response()->json(
           [
            'status' => 'success',
            'data' => $data
            ]
        );




    }
}
