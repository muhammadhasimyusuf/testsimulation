<?php

namespace App\Http\Controllers;

use App\Models\Answer;
use App\Models\Question;
use Illuminate\Http\Request;

class QuestionController extends Controller
{
    public function store(Request $request)
    {
        $data = Question::create($request->all());
        return response()->json(['message' => 'data added', 'data' => $data]);
    }


    public function getquestion($type)
    {
       $data = Question::select('question', 'no', 'type')->where('type', $type)
                                                ->limit(50)
                                                ->with('listchoice')
                                                ->paginate(1);

       return response()->json(["result" => (object)$data, "status" => "SUCCESS","response" => 200, "error" => false,],200);
    }

    public function answer(Request $request, $qno){

        $getAnwered = Answer::where('nim', $request->nim)->where('qno', $qno)->first();

        if($getAnwered){
            $getAnwered->update([
                "qlist" => $request->qlist,
                "qanswer" => $request->qanswer
            ]);
            return response()->json(["result" => $getAnwered, "status" => "UPDATED","response" => 200, "error" => false,],200);
        }

            $answer = Answer::create($request->all());
            return response()->json(["result" => $answer, "status" => "SUCCESS","response" => 200, "error" => false,],200);


    }
}
