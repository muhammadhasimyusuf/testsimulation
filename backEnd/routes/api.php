<?php

use App\Http\Controllers\MainController;
use App\Http\Controllers\QuestionController;
use App\Http\Controllers\UserController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "api" middleware group. Make something great!
|
*/




Route::get('/', [MainController::class, 'main']);

Route::post('/makequestion', [QuestionController::class, 'store']);

Route::get('/getquestion/{type}', [QuestionController::class, 'getquestion']);


Route::post('/user/add', [UserController::class, 'create']);

Route::post('/user/login', [UserController::class, 'login']);

Route::post('/answer/{qno}', [QuestionController::class, 'answer']);

Route::get('/answered/{nim}/{qno}', [UserController::class, 'getanswered']);

Route::post('/finish/{nim}', [UserController::class, 'finishtest']);

Route::get('/scores/{nim}', [UserController::class, 'getscore']);
