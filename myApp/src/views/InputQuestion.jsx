import React, { useRef } from 'react';
import QuestionCreate from '../models/Question/QuestionCreate';

function InputQuestion() {
 const question = useRef()
 const type = useRef()
 const answer = useRef()

 const {inputData} = QuestionCreate()

  const handleSubmit = (e) => {
      e.preventDefault()
      const data = {
        'question' : question.current.value,
        'type' : type.current.value,
        'answer' : answer.current.value,
      }
      console.log(data)
      inputData(data)
  }

  return (
    
    <main>
    <div className="px-4 bg-white min-h-screen sm:px-6 lg:px-8 py-8 w-full pt-40 max-w-9xl mx-auto">
      <div className="flex flex-col justify-center items-center mb-4">
        <h1 className="text-4xl text-black font-bold mb-10">Tambahkan Soal</h1>
        {/* <p className="text-gray-700">Please enter your credentials to log in.</p> */}
      </div>
      <center>
      <div className="rounded-lg p-8 max-w-2xl mx-auto">
        <form onSubmit={handleSubmit}>
          <div className=''>
          <div className="mb-7 w-1/2">
            <label className="text-left block text-gray-700 font-bold mb-2" htmlFor="soal">
              Soal
            </label>
            <input
              className="shadow appearance-none border rounded-lg w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline"
              id="Soal"
              type="text"
              ref={question}
              required
            />
          </div>
          <div className="mb-7 w-1/2">
            <label className="text-left block text-gray-700 font-bold mb-2" htmlFor="type">
              Type
            </label>
            <select
              className="shadow appearance-none border rounded-lg w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline"
              id="Type"
              type="text"
              ref={type}
              required
            >
                <option name="" id="" value={''}>Pilih type</option>
                <option name="" id="" value={'a'}>A</option>
                <option name="" id="" value={'b'}>B</option>
            </select>
          </div>
          <div className="mb-7 w-1/2">
            <label className="text-left block text-gray-700 font-bold mb-2" htmlFor="jawaban">
              Jawaban
            </label>
            <input
            ref={answer}
            required
              className="shadow appearance-none border rounded-lg w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline"
              id="jawaban"
              type="text"
            />
          </div>
          <div className="mb-3 w-1/2">
            {/* <label className="block text-gray-700 font-bold mb-2" htmlFor="password">
              Password
            </label> */}
            <input
              className="shadow appearance-none border rounded-lg w-full py-2 px-3 text-white bg-blue-600 leading-tight focus:outline-none focus:shadow-outline"
         
              type="submit"
              value="Input"
              placeholder="Password"
            />
          </div>
          <div className="mb-4 w-1/2">
            {/* <label className="block text-gray-700 font-bold mb-2" htmlFor="password">
              Password
            </label> */}
            {/* <a
              className="shadow appearance-none border rounded-lg w-full py-2 px-3 text-white bg-blue-600 leading-tight focus:outline-none focus:shadow-outline"
              id="password"
              type="submit"
              value="Daftar"
              placeholder="Password"
            /> */}
          </div>
          </div>
        </form>
      </div>
      </center>
    </div>
    </main>
  );
}

export default InputQuestion;
