import React from 'react'
import { Link, useNavigate } from 'react-router-dom';
import useAuth from '../hooks/AuthProvider';

export default function Header({children}) {
    const {user, signOut} = useAuth()
    const handleSignOut = () => {
        signOut()
    }
console.log(user)
  return (
    <>
    <div className='fixed bg-gray-600 w-[100%]'>
        <span className='flex justify-between py-3 px-3 text-white'>
        <span className='flex justify-start'>
            <span>
                <p>{user?.name}</p>
            </span>
        </span>
        <span className='flex justify-end'>
          <span onClick={handleSignOut} className='hover:cursor-pointer'>Sign Out</span>
        </span>
        </span>
    </div>
    <main>
        {children}
    </main>
    </>
  )
}
