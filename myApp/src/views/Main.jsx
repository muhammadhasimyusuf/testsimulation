import React from 'react'
import Test from '../models/Test'

function Main() {
  const {result} = Test()

  console.log(result)


  return (
    <div className='h-20 w-20 border bg-gray-100'>Main</div>
  ) 
}

export default Main