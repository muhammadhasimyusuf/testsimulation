import React from 'react'
import ReactDOM from 'react-dom/client'
import App from './App'
import { BrowserRouter as Router } from "react-router-dom";   
import './index.css'
import { AuthProvider } from './hooks/AuthProvider';

ReactDOM.createRoot(document.getElementById('root')).render(
  <Router>
    <AuthProvider>
    <App />
  </AuthProvider>
  </Router>
)
