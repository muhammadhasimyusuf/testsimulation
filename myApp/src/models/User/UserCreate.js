import axios from 'axios';
import { useState, useEffect } from "react";


function UserCreate(){
    
    const [result, setResult] = useState(null);
    const [loading, setLoading] = useState(false);
    const [error, setError] = useState(false);

    
    const Base_URL = import.meta.env.VITE_BASE_API

    const inputData = (payload) => {
        console.log({payload})
          let formdata = new URLSearchParams();
          formdata.append("name", payload.name);
          formdata.append("nim", payload.nim);
          formdata.append("qtype", payload.qtype);
          formdata.append("password", payload.password);
          setLoading(true);
          axios({
              method: 'POST',
              url: `${Base_URL}/user/add`,
              data: formdata,
            }).then((response) => {
                setResult(response.data);
          }).catch((err)=>{
              setError(err);
          }).finally(()=>{
              setLoading(false);
          });
      }
    
    return { inputData, result, loading, error,};
}

export default UserCreate