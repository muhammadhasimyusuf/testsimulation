import axios from 'axios';
import { useState, useEffect } from "react";
import { Link, useNavigate } from 'react-router-dom';


function FinishTest(){
    
    const [resultFinish, setResultFinish] = useState(null);
    const [loading, setLoading] = useState(false);
    const [error, setError] = useState(false);
    const navigate = useNavigate()

    
    const Base_URL = import.meta.env.VITE_BASE_API

    const Finish = (nim) => {
          setLoading(true);
          axios({
              method: 'POST',
              url: `${Base_URL}/finish/${nim}`,
            }).then((response) => {
                setResultFinish(response.data);
          }).catch((err)=>{
              setError(err);
          }).finally(()=>{
              setLoading(false);
            //   navigate('/inputQuestion')
          });
      }
    
    return { Finish, resultFinish, loading, error,};
}

export default FinishTest