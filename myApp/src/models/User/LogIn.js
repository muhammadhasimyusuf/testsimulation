import axios from 'axios';
import { useState, useEffect } from "react";
import { Link, useNavigate } from 'react-router-dom';


function LogIn(){
    
    const [result, setResult] = useState(null);
    const [loading, setLoading] = useState(false);
    const [error, setError] = useState(false);
    const navigate = useNavigate()

    
    const Base_URL = import.meta.env.VITE_BASE_API

    const inputData = (payload) => {
          let formdata = new URLSearchParams();
          formdata.append("nim", payload.nim);
          formdata.append("password", payload.password);
          setLoading(true);
          axios({
              method: 'POST',
              url: `${Base_URL}/user/login`,
              data: formdata,
            }).then((response) => {
                setResult(response.data);
          }).catch((err)=>{
              setError(err);
          }).finally(()=>{
              setLoading(false);
            //   navigate('/inputQuestion')
          });
      }
    
    return { inputData, result, loading, error,};
}

export default LogIn