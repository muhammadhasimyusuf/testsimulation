import axios from 'axios';
import { useState, useEffect } from "react";


function QuestionGet(){
    
    const [result, setResult] = useState(null);
    const [loading, setLoading] = useState(false);
    const [error, setError] = useState(false);

    
    const Base_URL = import.meta.env.VITE_BASE_API

    const Question = (type) => {
        //   let formdata = new URLSearchParams();
        
          setLoading(true);
          axios({
              method: 'GET',
              url: `${Base_URL}/getquestion/${type}`,
            //   data: formdata,
            }).then((response) => {
                setResult(response?.data);
          }).catch((err)=>{
              setError(err);
          }).finally(()=>{
              setLoading(false);
          });
      }

    const reQuestion = (url) => {
        //   let formdata = new URLSearchParams();
        
          setLoading(true);
          axios({
              method: 'GET',
              url: url,
            //   data: formdata,
            }).then((response) => {
                setResult(response?.data);
          }).catch((err)=>{
              setError(err);
          }).finally(()=>{
              setLoading(false);
          });
      }
    
    return { Question, reQuestion, result, loading, error,};
}

export default QuestionGet