import axios from 'axios';
import { useState, useEffect } from "react";


function QuestionCreate(){
    
    const [result, setResult] = useState(null);
    const [loading, setLoading] = useState(false);
    const [error, setError] = useState(false);

    
    const Base_URL = import.meta.env.VITE_BASE_API

    const inputData = (payload) => {
          let formdata = new URLSearchParams();
          formdata.append("question", payload.question);
          formdata.append("type", payload.type);
          formdata.append("answer", payload.answer);
          formdata.append("no", 100);
          setLoading(true);
          axios({
              method: 'POST',
              url: `${Base_URL}/makequestion`,
              data: formdata,
            }).then((response) => {
                setResult(response.data);
          }).catch((err)=>{
              setError(err);
          }).finally(()=>{
              setLoading(false);
          });
      }
    
    return { inputData, result, loading, error,};
}

export default QuestionCreate